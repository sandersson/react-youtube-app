import React from 'react';
import CSSTransitionGroup from 'react-addons-css-transition-group';
import { youtubeDurationToSeconds } from '../helpers.js';
import {SortableContainer, SortableElement} from 'react-sortable-hoc';

class Queue extends React.Component {
	constructor() {
		super();
		this.onSortEnd = this.onSortEnd.bind(this);

		window.addEventListener('touchstart', function() {
			document.body.className = "touch";
		});

	}

	handleRemove(e, key) {
		e.stopPropagation();
		this.props.deleteIndex(key);
	}

	onSortEnd(e) {
		this.props.handleMove(e);
	}

	render() {
		if (!this.props.active) return null;

		if (this.props.queue.length === 0 && this.props.history.length > 0) {
			return (
				<div className="app-queue-list">
					<button className="replay app-queue-replay" onClick={this.props.replayHistory}></button>
				</div>
			)
		}

		const SortableItem = SortableElement(({value}) => {
			const video = this.props.videos[value];
			if (!video) return;

			var classNames = "queue-item";

			//Add class for video status for first video in queue
			if (value === this.props.queue[0]) {
				classNames = `${classNames } queue-item--active`;
				classNames = `${classNames } queue-item--${this.props.status}`;
			}

			return (
				<li key={value} className={classNames} tabIndex="0" onClick={() => this.props.playVideo(value)} onKeyPress={() => this.props.playVideo(value)}>
					<h2 className="queue-item-title">{video.snippet.title}</h2>
					<p className="queue-item-duration">{youtubeDurationToSeconds(video.contentDetails.duration)}</p>
					<button onClick={(e) => this.handleRemove(e, value)} className="queue-item-remove"><span>x</span></button>
				</li>
			)

		});

		const SortableList = SortableContainer(({items}) => {
			return (
				<CSSTransitionGroup
				className="app-queue-list"
				component="ul"
				transitionName="queue"
				transitionEnterTimeout={500}
				transitionLeaveTimeout={500}
				>
					{items.map((value, index) => (
						<SortableItem key={`item-${index}`} index={index} value={value} />
					))}
				</CSSTransitionGroup>
			);
		});
	    return <SortableList items={this.props.queue} onSortEnd={this.onSortEnd} pressDelay={200} />;

		
	}
}

Queue.propTypes = {
	active: React.PropTypes.bool.isRequired,
	queue: React.PropTypes.array.isRequired,
	videos: React.PropTypes.object.isRequired,
	playVideo: React.PropTypes.func.isRequired,
	status: React.PropTypes.string.isRequired,
	deleteIndex: React.PropTypes.func.isRequired,
	replayHistory: React.PropTypes.func.isRequired,
	handleMove: React.PropTypes.func.isRequired
}


export default Queue;
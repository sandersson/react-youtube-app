import React from 'react';

class Dashboard extends React.Component {
	constructor() {
		super();
		this.goToBrowse = this.goToBrowse.bind(this);
	}

	goToBrowse(theme) {
		//Prevent form to submit
		event.preventDefault();

		//Transition to /browse
		this.context.router.transitionTo(`/browse`);
	}

	render(){
		return(
			<div className="dashboard">
				<h1 className="dashboard-title">Youtube Jukebox</h1>
				<button className="dashboard-browse" onClick={ (e) => this.goToBrowse(e) }>Start browsing!</button>
				{/**
					<h2 className="dashboard-subtitle">Or check out one of these playlists</h2>
					<ul className="dashboard-list">
						<li className="dashboard-list-item">Coming soon</li>
						<li className="dashboard-list-item">Coming soon</li>
						<li className="dashboard-list-item">Coming soon</li>
						<li className="dashboard-list-item">Coming soon</li>
					</ul>
				**/}
			</div>
		)
	}
}

Dashboard.contextTypes = {
	router: React.PropTypes.object
}

export default Dashboard;
import React from 'react';

class Header extends React.Component {
	constructor() {
		super();
		this.handleChange = this.handleChange.bind(this);
		this.search = this.search.bind(this);
		this.timer = null;
	}

	handleChange(e) {
		clearTimeout(this.timer);
		this.timer = setTimeout(this.search, 500);
	}

	search(e) {
		if (e) e.preventDefault();
		const q = this.queryInput.value;
		if (q.length >= 2)
			this.props.search(q);
	}

	render() {
		return (
			<header className="app-header">
				{/*<button className="app-header-back" onClick={this.props.goBack}>
					<span className="icon icon--left"></span>
				</button>*/}

				<div className="container app-header-container">
					<form className="app-search" onSubmit={ (e) => this.search(e) }>
						<input type="search" className="app-search-input" placeholder="Search.." ref={(input) => { this.queryInput = input }} onChange={(e) => this.handleChange(e)}/>
					</form>
				</div>
			</header>
		)
	}
}

Header.propTypes = {
	search:React.PropTypes.func.isRequired,
	goBack: React.PropTypes.func.isRequired
}

export default Header;
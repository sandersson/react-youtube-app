import React from 'react';

class NotFound extends React.Component {
	constructor() {
		super();
		this.goBack = this.goBack.bind(this);
	}

	goBack() {
		this.context.router.transitionTo(`/`);
	}

	render() {
		const backButton = <button className="app-header-back" onClick={this.goBack}><span className="icon icon--left">Go back</span></button>;

		return (
			<div>
				<header className="app-header">
					{backButton}
				</header>
				<h1 className="error"><strong>404</strong> NOT FOUND</h1>
			</div>
		)

	}
}

NotFound.contextTypes = {
	router: React.PropTypes.object
}

export default NotFound;
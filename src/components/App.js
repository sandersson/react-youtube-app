import React from 'react';
import axios from 'axios';
import SwipeableViews from 'react-swipeable-views';

import { move } from '../helpers.js';
import { arrayMove } from 'react-sortable-hoc';


import Header from './Header';
import Browse from './Browse';
import Queue from './Queue';
import Player from './Player';
import History from './History';

const styles = {
  slideContainer: {
    overflow:'visible',
    height:'100%'
  },
};


class App extends React.Component {
	constructor() {
		super();

		this.search = this.search.bind(this);
		this.goBack = this.goBack.bind(this);
		this.addToQueue = this.addToQueue.bind(this);
		this.playVideo = this.playVideo.bind(this);
		this.playNext = this.playNext.bind(this);
		this.setStatus = this.setStatus.bind(this);
		this.deleteIndex = this.deleteIndex.bind(this);
		this.handleMove = this.handleMove.bind(this);
		this.removeFromHistory = this.removeFromHistory.bind(this);
		this.renderToggle = this.renderToggle.bind(this);
		this.replayHistory = this.replayHistory.bind(this);

		//Set default state.. 
		//TODO: Maybe move Youtube-key somewhere else?
		this.state = {
			key: 'AIzaSyDYmAaUsdYs4EUBFo36ndm2g28GVySJB4A',
			truncate_limit: 300,
			items_per_page: 25,
			searchResult: {},
			queue: [],
			history: [],
			videos:{},
			status: "",
			view:"queue"
		}
	}

	componentDidMount() {
		//Check if there is anything saved in session storage
		const sessionStorageQueue = sessionStorage.getItem('queue');
		const sessionStorageHistory = sessionStorage.getItem('history');
		const sessionStorageVideos = sessionStorage.getItem('videos');
		const sessionStorageSearchResult = sessionStorage.getItem('searchResult');

		const queue = JSON.parse(sessionStorageQueue) ? JSON.parse(sessionStorageQueue) : [];
		const history = JSON.parse(sessionStorageHistory) ? JSON.parse(sessionStorageHistory) : [];
		const videos = JSON.parse(sessionStorageVideos) ? JSON.parse(sessionStorageVideos) : {};
		const searchResult = JSON.parse(sessionStorageSearchResult) ? JSON.parse(sessionStorageSearchResult) : {};
		// save to state
		this.setState({ queue, history, videos, searchResult });
	}

	componentWillUpdate(nextProps, nextState) {
		sessionStorage.setItem('queue', JSON.stringify(nextState.queue));
		sessionStorage.setItem('history', JSON.stringify(nextState.history));
		sessionStorage.setItem('videos', JSON.stringify(nextState.videos));
		sessionStorage.setItem('searchResult', JSON.stringify(nextState.searchResult));
	}

	goBack() {
		//Go back to root
		this.context.router.transitionTo(`/`);
	}

	search(q) {
		//Search Youtube with the selected query
		axios.get(`https://www.googleapis.com/youtube/v3/search?part=snippet&type=video&maxResults=${this.state.items_per_page}&q=${q}&key=${this.state.key}`)
			.then(result => {
				//Save searchResult in state
				//const searchResult = result.data.items.map(obj => obj);
				const searchResult = {};
				result.data.items.map(function(obj){
					searchResult[obj.id.videoId] = obj;
					return true;
				});
				this.setState({ searchResult });
			}
		);
	}

	addToQueue(vId) {
		//List items in playlist and add to queue
		const queue = this.state.queue;
		const videos = {...this.state.videos};
		const searchResult = {...this.state.searchResult};

		axios.get(`https://www.googleapis.com/youtube/v3/videos?part=snippet,contentDetails&id=${vId}&key=${this.state.key}`)
			.then(result => {
				//Remove from searchresult and save in state
				searchResult[vId] = null;

				//Add to queue and save in state
				const timestamp = Date.now();
				queue.push(timestamp);
				
				const video = result.data.items[0];
				videos[timestamp] = video;

				this.setState({ searchResult, queue, videos });
			}
		);
	}

	deleteIndex(timestamp){
		var queue = this.state.queue;
		var index = queue.map(function(x) {return x; }).indexOf(timestamp);

		queue.splice(index, 1);
		this.setState({ queue });
	}

	removeFromHistory(timestamp) {
		var history = this.state.history;
		var index = history.map(function(x) {return x; }).indexOf(timestamp);

		history.splice(index, 1);
		this.setState({ history });
	}

	playVideo(timestamp) {
		var queue = this.state.queue;
		var history = this.state.history;
		var index = queue.map(function(x) {return x; }).indexOf(timestamp);

		if (index === 0) return;

		history.unshift(queue[0]);
		queue = move(queue, index, 1);
		queue.splice(0, 1);
		this.setState({queue, history});
	}

	playNext(key) {
		//Remove first object in queue to play next video
		const queue = this.state.queue;
		var history = this.state.history;
		history.unshift(queue[0]);
		queue.shift();

		this.setState({ queue, history });
	}

	setStatus(status) {
		switch (status) {
			case 1:
				status = 'playing';
				break;
			case 2:
				status = 'paused';
				break;
			case 3:
				status = 'loading';
				break;
			case 4:
				status = '4';
				break;
			default:
				return;
		}
		this.setState({ status });
	}

	handleMove(obj) {
		//Save new queue order in state
		var queue = this.state.queue;
		this.setState({
			queue: arrayMove(queue, obj.oldIndex, obj.newIndex)
		});
	}

	toggleView(e) {
		//Toggle Queue/History
		const view = e.title
		this.setState({view});
	}

	replayHistory() {
		var history = this.state.history;
		var queue = history;
		history = [];

		this.setState({queue, history});
	}

	renderToggle(title) {
		var className = 'app-toggle-btn';
		if (this.state.view === title)
			className = `${className} app-toggle-btn--active`
		return (
			<button className={className} onClick={(e) => this.toggleView({title})}>{title}</button>
		)
	}

	render(){
		return(
			<div>
				<Header
					search={this.search}
					goBack={this.goBack}
				/>
				
				{/* Todo: disable swipe on desktop instead of CSS-hack */}
				<SwipeableViews className="app" slideStyle={styles.slideContainer} enableMouseEvents>
					<div className="app-swiper app-browse">
						<Browse
							items={this.state.searchResult}
							queue={this.state.queue}
							videos={this.state.videos}
							addToQueue={this.addToQueue}
						/>
					</div>
					<div className="app-swiper app-queue">
						<div className="app-queue-container">
							<Player
								queue={this.state.queue}
								videos={this.state.videos}
								playNext={this.playNext}
								setStatus={this.setStatus}
							/>
							<Queue
								queue={this.state.queue}
								history={this.state.history}
								videos={this.state.videos}
								playVideo={this.playVideo}
								status={this.state.status}
								deleteIndex={this.deleteIndex}
								handleMove={this.handleMove}
								replayHistory={this.replayHistory}
								active={this.state.view === 'queue' ? true : false}
							/>
							<History
								active={this.state.view === 'history' ? true : false}
								videos={this.state.videos}
								history={this.state.history}
								removeFromHistory={this.removeFromHistory}
							/>
							<div className="app-toggle">
								{this.renderToggle('queue')}
								{this.renderToggle('history')}
							</div>
						</div>
					</div>
				</SwipeableViews>
			</div>
			
		)
	}
}

App.propTypes = {
	params: React.PropTypes.object.isRequired,
}
App.contextTypes = {
	router: React.PropTypes.object
}


export default App;
import React from 'react';
import CSSTransitionGroup from 'react-addons-css-transition-group';

class Browse extends React.Component {
	constructor() {
		super();
		this.selectItem = this.selectItem.bind(this);
		this.renderItem = this.renderItem.bind(this);

	}
	
	selectItem(vId, e) {
		if (vId) {
			if (!e || e.key === 'Enter')
	            this.props.addToQueue(vId);
		}
		return false;
	}

	renderItem(key) {
		const video = this.props.items[key];
		if (!video)
			return;

		var className = "browse-item";
		if (this.props.queue[key]) {
			className = `${className } browse-item--inqueue`;
		}

		return (
			<li key={video.id.videoId} className={className} tabIndex="0" onClick={(e) => this.selectItem(video.id.videoId)} onKeyPress={(e) => this.selectItem(video.id.videoId, e)}>
				<img className="browse-item-image" src={video.snippet.thumbnails.medium.url} alt={video.snippet.title} />
				<h2 className="browse-item-title">{video.snippet.title}</h2>
			</li>
		)
	}
	
	render() {
		const items = Object.keys(this.props.items);


		return (
			<CSSTransitionGroup
				className="browse-list"
				component="ul"
				transitionName="browse"
				transitionEnterTimeout={500}
				transitionLeaveTimeout={500}
			>
				{items.map(this.renderItem)}
			</CSSTransitionGroup>
		)

	}
}

Browse.propTypes = {
	items: React.PropTypes.object.isRequired,
	queue: React.PropTypes.array.isRequired,
	addToQueue: React.PropTypes.func.isRequired
}

export default Browse;
import React from 'react';
import YoutubePlayer from 'react-youtube';


class Player extends React.Component {

	constructor() {
		super();
		this.onEnd = this.onEnd.bind(this);
		this.onStateChange = this.onStateChange.bind(this);
	}

	onReady() {
		//console.log('onReady');
	}

	onPlay() {
		// console.log('onPlay');
	}

	onPause() {
		// console.log('onPause');
	}

	onEnd() {
		this.props.playNext();
	}

	onError() {
		// console.log('onError');
	}

	onPlaybackRateChange() {
		// console.log('onPlaybackRateChange');
	}

	onStateChange(e) {
		this.props.setStatus(e.data);
	}

	onPlaybackQualityChange() {
		// console.log('onPlaybackQualityChange');
	}

	render() {
		const opts = {
			playerVars: {
				autoplay: 1,
				modestbranding:true
			}
		};
		const video = this.props.queue[0] ? this.props.videos[this.props.queue[0]] : null;

		if (!video) {
			return (
				<div className="player"></div>
			)
		}

		return (
			<div className="player">
				<YoutubePlayer
				  videoId={video.id}
				  opts={opts}	              // defaults -> null 
				  className='player-iframe'                // defaults -> null 
				  onReady={this.onReady}                    // defaults -> noop 
				  onPlay={this.onPlay}                     // defaults -> noop 
				  onPause={this.onPause}                    // defaults -> noop 
				  onEnd={this.onEnd}                      // defaults -> noop 
				  onError={this.onError}                    // defaults -> noop 
				  onStateChange={this.onStateChange}              // defaults -> noop 
				  onPlaybackRateChange={this.onPlaybackRateChange}       // defaults -> noop 
				  onPlaybackQualityChange={this.onPlaybackQualityChange}    // defaults -> noop 
				/>
			</div>
		)
	}
}

Player.propTypes = {
	queue: React.PropTypes.array.isRequired,
	videos: React.PropTypes.object.isRequired,
	playNext: React.PropTypes.func.isRequired,
	setStatus: React.PropTypes.func.isRequired
}


export default Player;
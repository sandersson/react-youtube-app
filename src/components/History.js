import React from 'react';
import CSSTransitionGroup from 'react-addons-css-transition-group';

class History extends React.Component {
	constructor() {
		super();

		this.renderItem = this.renderItem.bind(this);

	}

	renderItem(key) {
		const video = this.props.videos[key];
		if (!video) return;

		var classNames = "queue-item queue-item--history";

		return (
			<li key={key} className={classNames} tabIndex="0">
				<h2 className="queue-item-title">{video.snippet.title}</h2>
				<button onClick={() => this.props.removeFromHistory(key)} className="queue-item-remove"><span>x</span></button>
			</li>
		)
	}

	render() {
		if (!this.props.active) return null;

		return (
			<CSSTransitionGroup
				className="app-queue-list"
				component="ul"
				transitionName="queue"
				transitionEnterTimeout={500}
				transitionLeaveTimeout={500}
				>
					 {this.props.history.map(this.renderItem)}
				</CSSTransitionGroup>
		)

	}
}

History.propTypes = {
	active: React.PropTypes.bool.isRequired,
	history: React.PropTypes.array.isRequired,
	videos: React.PropTypes.object.isRequired,
	removeFromHistory: React.PropTypes.func.isRequired
}

export default History;
export function youtubeDurationToSeconds (duration) {
	var match = duration.match(/PT(\d+H)?(\d+M)?(\d+S)?/)

	var hours = (parseInt(match[1], 10) || 0);
	var minutes = (parseInt(match[2], 10) || 0);
	var seconds = (parseInt(match[3], 10) || 0);

	return fancyTimeFormat(hours * 3600 + minutes * 60 + seconds);

}

export function fancyTimeFormat(seconds) {   
	// Hours, minutes and seconds
	var hrs = ~~(seconds / 3600);
	var mins = ~~((seconds % 3600) / 60);
	var secs = seconds % 60;

	// Output like "1:01" or "4:03:59" or "123:03:59"
	var ret = "";

	if (hrs > 0) {
		ret += "" + hrs + ":" + (mins < 10 ? "0" : "");
	}

	ret += "" + mins + ":" + (secs < 10 ? "0" : "");
	ret += "" + secs;
	return ret;
}

export function move(arr, old_index, new_index) {
	while (old_index < 0) {
		old_index += arr.length;
	}
	while (new_index < 0) {
		new_index += arr.length;
	}
	if (new_index >= arr.length) {
		var k = new_index - arr.length;
		while ((k--) + 1) {
			arr.push(undefined);
		}
	}
	arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);  
	return arr;
}
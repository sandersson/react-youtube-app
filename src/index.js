import React from 'react';
import  { render } from 'react-dom';
import { BrowserRouter, Match, Miss } from 'react-router';

import './css/style.css';

import App from './components/App';
//import Dashboard from './components/Dashboard';
import NotFound from './components/NotFound';

const Root = () => {

	return (
		<BrowserRouter>
			<div>
				{/* Check if is root and go to APP */ }
				<Match exactly pattern="/" component={App} />

				{/* Check if theme is selected and to to App 
				<Match pattern="/browse" component={App} />
				*/ }
				{/* 404 */ }
				<Miss component={NotFound}/>
			</div>
		</BrowserRouter>
	)
}

render(<Root/>, document.querySelector('#main'))